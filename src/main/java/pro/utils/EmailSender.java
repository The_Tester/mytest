package pro.utils;

import java.util.Properties;
import javax.mail.*;
import javax.mail.internet.*;

public class EmailSender{
    private String body = "In test \n";

    public void sendWithBody(String toEmail, String body){
        try {
            this.body = body;
            send(toEmail);
        } catch (MessagingException e) {
            e.printStackTrace();
        }
    }

    public void send(String toEmail) throws MessagingException {
//        "java@intellij.pro"
        Properties properties = System.getProperties();
        properties.setProperty("mail.smtp.host", "smtp.yandex.ru");
        properties.setProperty("mail.smtp.port", "465");
        properties.setProperty("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        properties.setProperty("mail.smtp.socketFactory.port", "465");
        properties.setProperty("mail.smtp.auth", "true");

        Session session = Session.getInstance(properties,new Authenticator(){
            protected PasswordAuthentication getPasswordAuthentication(){
                return(new PasswordAuthentication("java@intellij.pro","ASDqwe123!"));
            }
        });

        MimeMessage message = new MimeMessage(session);
        message.setFrom(new InternetAddress("java@intellij.pro"));
        message.setRecipient(Message.RecipientType.TO, new InternetAddress(toEmail));

        message.setSubject("Subject","koi8-r");
        message.setText(body,"koi8-r");
        Transport.send(message);
    }
}
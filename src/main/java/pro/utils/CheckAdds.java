package pro.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * ____ ____ ____ ____ ___ ____ ___     ___  _   _    ___  ____ _  _ _ ____ ____
 * |    |__/ |___ |__|  |  |___ |  \    |__]  \_/     |  \ |___ |\ | | [__  [__
 * |___ |  \ |___ |  |  |  |___ |__/    |__]   |      |__/ |___ | \| | ___] ___]
 * 05/01/2018
 */
public class CheckAdds {

    public List<String> checkNewAdverds(){
        List<String> list = new ArrayList<String>();
        String source = "";
        try {
            source = getURLSource("https://www.donedeal.ie/dogs");
        }catch (IOException e){}

        list = pullLinks(source);

        return list;
    }

    private String getURLSource(String url) throws IOException{
        List<String> list = new ArrayList<String>();
        StringBuilder builder = new StringBuilder();

        URL yahoo = new URL(url);
        BufferedReader in = new BufferedReader(
                new InputStreamReader(
                        yahoo.openStream()));

        String inputLine;

        while ((inputLine = in.readLine()) != null) {
            builder.append(inputLine);
        }

        in.close();

        return builder.toString();
        }

    private List<String> pullLinks(String text) {
        List<String> links = new ArrayList<String>();

        String regex = "\\(?\\b(http://|www[.])[-A-Za-z0-9+&amp;amp;@#/%?=~_()|!:,.;]*[-A-Za-z0-9+&amp;amp;@#/%=~_()|]";
        Pattern p = Pattern.compile(regex);
        Matcher m = p.matcher(text);

        while(m.find()) {
            String urlStr = m.group();

            if (urlStr.indexOf("dogs-for-sale") >= 0){
                links.add(urlStr);
            }
        }
        return links;
    }
}

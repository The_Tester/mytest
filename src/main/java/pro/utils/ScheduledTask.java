package pro.utils;

import javax.mail.MessagingException;
import java.util.ArrayList;
import java.util.List;
import java.util.TimerTask;

/**
 * ____ ____ ____ ____ ___ ____ ___     ___  _   _    ___  ____ _  _ _ ____ ____
 * |    |__/ |___ |__|  |  |___ |  \    |__]  \_/     |  \ |___ |\ | | [__  [__
 * |___ |  \ |___ |  |  |  |___ |__/    |__]   |      |__/ |___ | \| | ___] ___]
 * 05/01/2018
 */
public class ScheduledTask extends TimerTask {

    CheckAdds checkAdds = new CheckAdds();
    List<String> listUrls = new ArrayList<String>();

    public void run() {
        List<String> newListUrls = checkAdds.checkNewAdverds();

        if (!listUrls.get(0).equals(newListUrls.get(0))) {
            EmailSender sender = new EmailSender();

            StringBuilder htmlBul = new StringBuilder("<!DOCTYPE html>\n <html>\n<body>");

            for (String url: newListUrls) {
               htmlBul.append("\n<a href=\"");
               htmlBul.append(url);
               htmlBul.append("\">LINK</a><br>");
            }

            htmlBul.append("</body>\n</html>");
            String body = htmlBul.toString();

            sender.sendWithBody("java@intellij.pro", body);

            listUrls.clear();
            listUrls.addAll(newListUrls);
        }
    }
}

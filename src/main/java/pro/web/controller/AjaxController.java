package pro.web.controller;

import com.fasterxml.jackson.annotation.JsonView;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import pro.utils.EmailSender;
import pro.web.jsonview.Views;
import pro.web.model.AjaxResponseBody;
import pro.web.model.SearchCriteria;
import pro.web.model.User;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

@RestController
public class AjaxController {

    List<User> users;

    @JsonView(Views.Public.class)
    @RequestMapping(value = "/search/api/getSearchResult")
    public AjaxResponseBody getSearchResultViaAjax(@RequestBody SearchCriteria search) {

        AjaxResponseBody result = new AjaxResponseBody();

        if (isValidSearchCriteria(search)) {
            List<User> users = findByUserNameOrEmail(search.getUsername(), search.getEmail());

            if (users.size() > 0) {
                result.setCode("200");
                result.setMsg("");
                result.setResult(users);
            } else {
                result.setCode("204");
                result.setMsg("No user!");
            }

        } else {
            result.setCode("400");
            result.setMsg("Search criteria is empty!");
        }

        //AjaxResponseBody will be converted into json format and send back to client.
        return result;

    }

    private boolean isValidSearchCriteria(SearchCriteria search) {

        boolean valid = true;

        if (search == null) {
            valid = false;
        }

        if ((StringUtils.isEmpty(search.getUsername())) && (StringUtils.isEmpty(search.getEmail()))) {
            valid = false;
        }

        return valid;
    }

    @PostConstruct
    private void iniDataForTesting() {
        users = new ArrayList<User>();

        User user1 = new User("mkyong", "pass123", "mkyong@yahoo.com", "012-1234567", "address 123");
        User user2 = new User("yflow", "pass456", "yflow@yahoo.com", "016-7654321", "address 456");
        User user3 = new User("laplap", "pass789", "mkyong@yahoo.com", "012-111111", "address 789");
        users.add(user1);
        users.add(user2);
        users.add(user3);

    }

    private List<User> findByUserNameOrEmail(String username, String email) {

        List<User> result = new ArrayList<User>();

        for (User user : users) {

            if ((!StringUtils.isEmpty(username)) && (!StringUtils.isEmpty(email))) {

                if (username.equals(user.getUsername()) && email.equals(user.getEmail())) {
                    result.add(user);
                    continue;
                } else {
                    continue;
                }

            }
            if (!StringUtils.isEmpty(username)) {
                if (username.equals(user.getUsername())) {
                    result.add(user);
                    continue;
                }
            }

            if (!StringUtils.isEmpty(email)) {
                if (email.equals(user.getEmail())) {
                    result.add(user);
                    continue;
                }
            }

        }

        return result;

    }

    @RequestMapping(value={"/{email:.+}"}, method= RequestMethod.GET)
    public @ResponseBody boolean mailSend(@PathVariable(value="email") String email){
        EmailSender sender = new EmailSender();
        try {
            sender.send(email);
            return true;
        }catch (Exception e){
            return false;
        }
    }
}
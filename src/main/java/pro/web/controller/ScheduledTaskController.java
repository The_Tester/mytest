package pro.web.controller;

import org.springframework.web.bind.annotation.*;
import pro.utils.ScheduledTask;

import java.util.Timer;

/**
 * ____ ____ ____ ____ ___ ____ ___     ___  _   _    ___  ____ _  _ _ ____ ____
 * |    |__/ |___ |__|  |  |___ |  \    |__]  \_/     |  \ |___ |\ | | [__  [__
 * |___ |  \ |___ |  |  |  |___ |__/    |__]   |      |__/ |___ | \| | ___] ___]
 * 05/01/2018
 */
@RestController
public class ScheduledTaskController {

    @RequestMapping(value={"/start"}, method= RequestMethod.GET)
    public @ResponseBody String startScheduledTask(){

        Timer time = new Timer(); // Instantiate Timer Object

        ScheduledTask st = new ScheduledTask(); // Instantiate SheduledTask class

        time.schedule(st, 0, 1 * 60 * 1000); // Create Repetitively task for every 1 secs

        return "RUN";
    }
}
